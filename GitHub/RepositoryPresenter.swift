//
//  RepositoryPresenter.swift
//  GitHub
//
//  Created by Bruno Alves on 26/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import Foundation
import Alamofire

class RepositoryPresenter {
    
    static func fetchRepositories(completion:@escaping ([AnyObject]?) -> Void){
        
        let endpoint = "\(Endpoint.Repositories.get)&page=1"
        
        let request = Alamofire.request(endpoint, method: .get)
        request.responseJSON { (response) in
            
            let JSON = response.result.value as? [String:AnyObject]
            let items = JSON?["items"] as? [AnyObject]
            completion(items)
        }
    }
    
}
