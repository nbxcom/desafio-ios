//
//  MainSplitViewController.swift
//  GitHub
//
//  Created by Bruno Alves on 19/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import UIKit

class MainSplitViewController: UISplitViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.splitViewController?.preferredDisplayMode = .allVisible
    }
    
    
}
