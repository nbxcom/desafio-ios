//
//  RepositoriesTableViewController.swift
//  GitHub
//
//  Created by Bruno Alves on 22/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import UIKit

class RepositoriesTableViewController: UITableViewController {
    
    var Repositories = [Repository]()
    var SelectedRepository:Repository? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        RepositoryPresenter.fetchRepositories { (_repo) in
            
            for repo in _repo! {
                
                let owner = repo["owner"] as! Dictionary<String,AnyObject>
                
                self.Repositories.append(Repository(name: repo["name"] as! String, description: repo["description"] as! String, fork: repo["forks"]as! Int, star: repo["stargazers_count"] as! Int, ownerUserName: owner["login"] as! String))
                
            }
            
            self.tableView.reloadData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Repositories.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoryTableViewCell
        
        cell.RepoName.text = Repositories[indexPath.row].getName()
        cell.Description.text = Repositories[indexPath.row].getDescription()
        cell.Fork.text = "Forks: \(Repositories[indexPath.row].getFork())"
        cell.Star.text = "Star: \(Repositories[indexPath.row].getStar())"
        
        // Configure the cell...
        return cell
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowDetail" {
            
            let indexPath = self.tableView.indexPathForSelectedRow
            self.SelectedRepository = Repositories[(indexPath?.row)!]
            let controller = (segue.destination as! UINavigationController).topViewController as! PullRequestsTableViewController
            controller.repository = self.SelectedRepository
            controller.title = self.SelectedRepository?.getName()
            SelectedRepository = nil
            
        }
        
    }
    
}
