//
//  PullRequest.swift
//  GitHub
//
//  Created by Bruno Alves on 26/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import Foundation
import Alamofire

class PullRequestPresenter {
    
    static func fetchRepositoryPulls(repository: Repository, completion:@escaping ([AnyObject]?) -> Void){
        
        let creator = repository.getOwnerUserName()
        let repositoryName = repository.getName()
        
        let endpoint = "\(Endpoint.Pulls.get)/\(creator)/\(repositoryName)/pulls"
        
        let request = Alamofire.request(endpoint, method: .get)
        request.responseJSON { (response) in
            
            let JSON = response.result.value as? [AnyObject]
            NSLog("\(endpoint)")
            completion(JSON)
        }
    }
    
}
