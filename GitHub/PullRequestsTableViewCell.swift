//
//  PullRequestsTableViewCell.swift
//  GitHub
//
//  Created by Bruno Alves on 26/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import UIKit

class PullRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var Content: UIView!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Description: UILabel!
    @IBOutlet weak var Username: UILabel!
    @IBOutlet weak var Avatar: UIImageView!
    @IBOutlet weak var Date: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
