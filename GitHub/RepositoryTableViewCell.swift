//
//  RepositoryTableViewCell.swift
//  GitHub
//
//  Created by Bruno Alves on 22/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    @IBOutlet weak var RepoName: UILabel!
    @IBOutlet weak var Description: UILabel!
    @IBOutlet weak var Fork: UILabel!
    @IBOutlet weak var Star: UILabel!
    @IBOutlet weak var Content: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
