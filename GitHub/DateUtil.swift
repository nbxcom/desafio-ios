//
//  DateUtil.swift
//  GitHub
//
//  Created by Bruno Alves on 29/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import Foundation

class DateUtil {
    
    static func convertDateFormater(date: String, format: String) -> String {
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/mm/yyyy"
        
        let date: NSDate? = dateFormatterGet.date(from: date) as NSDate?
        let dateOut = dateFormatterPrint.string(from: date as! Date)
        
        return dateOut
    }
    
}
