//
//  PullRequestsTableViewController.swift
//  GitHub
//
//  Created by Bruno Alves on 26/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import UIKit
import AlamofireImage

class PullRequestsTableViewController: UITableViewController {
    
    var Pulls = [PullRequest]()
    var repository: Repository!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if repository != nil {
            
            PullRequestPresenter.fetchRepositoryPulls(repository: repository) { (_pulls) in
                
                for pull in _pulls! {
                    
                    let user = pull["user"] as! [String:AnyObject]
                    
                    self.Pulls.append(PullRequest(title: pull["title"] as! String, body: pull["body"] as! String, user: user["login"] as! String, userAvatar: user["avatar_url"] as! String, date: pull["created_at"] as! String))
                    
                }
                self.tableView.reloadData()
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.Pulls.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequest", for: indexPath) as! PullRequestsTableViewCell
        
        cell.Name.text = Pulls[indexPath.row].getTitle()
        cell.Description.text = Pulls[indexPath.row].getBody()
        cell.Username.text = Pulls[indexPath.row].getUser()
        let userAvatar = Pulls[indexPath.row].getUserAvatar()
        let url = NSURL(string: userAvatar)
        
        cell.Avatar.af_setImage(withURL: url as! URL)
        
        //Rounded avatar
        cell.Avatar.layer.borderWidth = 1
        cell.Avatar.layer.masksToBounds = false
        cell.Avatar.layer.borderColor = UIColor.black.cgColor
        cell.Avatar.layer.cornerRadius = cell.Avatar.frame.height/2
        cell.Avatar.clipsToBounds = true
        
        cell.Date.text = Pulls[indexPath.row].getDate()
        
        return cell
        
    }

}
