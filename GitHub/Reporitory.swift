//
//  Reporitory.swift
//  GitHub
//
//  Created by Bruno Alves on 22/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import Foundation

struct Repository {
    
    private var name:String
    private var description: String
    private var fork: Int
    private var star: Int
    private var ownerUserName: String
    
    init(name:String, description: String, fork: Int, star: Int, ownerUserName: String) {
        
        self.name = name
        self.description = description
        self.fork = fork
        self.star = star
        self.ownerUserName = ownerUserName
        
    }
    
    func getName() -> String { return self.name }
    func getDescription() -> String { return self.description }
    func getFork() -> Int { return self.fork }
    func getStar() -> Int { return self.star }
    func getOwnerUserName() -> String { return self.ownerUserName }
    
}
