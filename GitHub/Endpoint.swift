//
//  Endpoint.swift
//  GitHub
//
//  Created by Bruno Alves on 26/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import Foundation

enum Endpoint {
    
    case Repositories, Pulls
    
    var get:String {
        
        switch self {
        case .Repositories:
            return "https://api.github.com/search/repositories?q=language:Java&sort=stars"
        case .Pulls:
            return "https://api.github.com/repos"
        }
    }
}
