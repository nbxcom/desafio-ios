//
//  PullRequest.swift
//  GitHub
//
//  Created by Bruno Alves on 26/05/17.
//  Copyright © 2017 NBX. All rights reserved.
//

import Foundation

struct PullRequest {
    
    var title: String
    var body: String
    var user: String
    var userAvatar: String
    var date: String
    
    init(title: String, body: String, user: String, userAvatar:String, date: String) {
        
        self.title = title
        self.body = body
        self.user = user
        self.userAvatar = userAvatar
        self.date = DateUtil.convertDateFormater(date: date, format: "dd/mm/yyyy")
        
    }
    
    func getTitle() -> String { return self.title }
    func getBody() -> String { return self.body }
    func getUser() -> String { return self.user }
    func getUserAvatar() -> String { return self.userAvatar }
    func getDate() -> String { return self.date }
    
}
